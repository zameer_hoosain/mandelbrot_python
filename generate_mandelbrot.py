from Tkinter import *

master = Tk()
canv = None

WIDTH = 350
HEIGHT = 200

canv = Canvas(master, width=WIDTH, height=HEIGHT)
canv.pack()
img = PhotoImage(width=WIDTH, height=HEIGHT)
canv.create_image((WIDTH/2, HEIGHT/2), image=img, state="normal")

for p_y in range(-100, 100, 1):
	for p_x in range(-250, 100, 1):
		x0 = p_x/100.0
		y0 = p_y/100.0

		x = 0
		y = 0

		iteration = 0
		max_iteration = 250
		while (x**2 + y**2 < 4 and iteration < max_iteration):
			x_temp = x**2 - y**2 + x0
			y = 2*x*y + y0
			x = x_temp
			iteration += 1

		if iteration == max_iteration:
			img.put('#000000', (p_x+250, p_y+100))
		else:
			img.put('#ffffff', (p_x+250, p_y+100))

master.mainloop()
